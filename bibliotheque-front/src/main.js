import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VueFlashMessage from 'vue-flash-message';
import App from './App'
import Home from './components/Home'
import Usagers from './components/usagers/Usagers'
import Oeuvres from "./components/oeuvres/Oeuvres";
import Auteurs from "./components/auteurs/Auteurs";
import Emprunts from "./components/emprunts/Emprunts";
import Reservations from "./components/reservations/Reservations";
import NotFound from './components/NotFound'

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(VueFlashMessage, {
  messageOptions: {
    timeout: 2000
  }
});

import './assets/fontawesome-all.js'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home, name: "root" },
    { path: '/usagers', component: Usagers, name: "usagers" },
    { path: '/oeuvres', component: Oeuvres, name: "oeuvres" },
    { path: '/auteurs', component: Auteurs, name: "auteurs" },
    { path: '/emprunts', component: Emprunts, name: "emprunts" },
    { path: '/reservations', component: Reservations, name: "reservations" },
    { path: '*', component: NotFound }
  ]
})

require('vue-flash-message/dist/vue-flash-message.min.css');

Vue.config.productionTip = false

Vue.mixin({
  data() {
    return {
      get apiRoot() {
        return `${process.env.VUE_APP_API_HOST}:${process.env.VUE_APP_API_PORT}`;
      }
    }
  }
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
