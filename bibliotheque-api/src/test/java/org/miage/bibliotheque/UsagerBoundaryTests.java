package org.miage.bibliotheque;

import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
public class UsagerBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UsagerService usagerService;

    @Before
    public void reset() {
        usagerService.deleteAll();
    }

    @Test
    public void getOne_ShouldReturnOneRecord_IfIdExists() {
        var usager = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(usager);
        var responseEntity = restTemplate.getForEntity("/usagers/" + usager.getId(), String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).contains(usager.getId());
    }

    @Test
    public void getOne_ShouldFail_IfIdDoesNotExist() {
        var responseEntity = restTemplate.getForEntity("/usagers/" + "doesntexist", String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void getAll_ShouldReturnAllRecords() {
        var u1 = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(u1);

        var u2 = new Usager("fizzbuzz@email.com", "buzz", "fizz", "+33 6 09 87 65 43");
        usagerService.save(u2);

        var responseEntity = restTemplate.getForEntity("/usagers", String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).contains(u1.getId());
        assertThat(responseEntity.getBody()).contains(u2.getId());
    }

    @Test
    public void store_ShouldSucceed_IfRequestComplies() {
        var usager = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        var postResponse = restTemplate.postForEntity("/usagers", usager, String.class);

        assertThat(postResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(usagerService.findByExample(usager).isPresent()).isTrue();
    }

    @Test
    public void store_ShouldFail_IfAnyAttributeIsNull() {
        var usager = new Usager(null, "bar", "foo", "0600000000");
    }

    @Test
    public void store_ShouldFail_IfEmailNotAvailable() {
        var email = "foobar@email.com";
        var usager = new Usager(email, "buzz", "fizz", "+33 6 00 00 00 00");
        usagerService.save(usager);

        usager = new Usager(email, "bar", "foo", "+33 6 12 34 56 78");
        var responseEntity = restTemplate.postForEntity("/usagers", usager, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    public void update_ShouldSucceed_IfRequestComplies() {
        var usager = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(usager);

        var nouveauNom = "buzz";
        usager.setNom(nouveauNom);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(usager, headers);
        var responseEntity = restTemplate.exchange("/usagers/" + usager.getId(), HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(usagerService.findById(usager.getId())).isNotEmpty();
        assertThat(usagerService.findById(usager.getId()).get().getNom()).isEqualTo(nouveauNom);
    }

    @Test
    public void update_ShouldFail_IfRequestBodyIsEmpty() {
        var usager = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(usager);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(null, headers);
        var responseEntity = restTemplate.exchange("/usagers/" + usager.getId(), HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void update_ShouldFail_IfIdDoesNotExist() {
        var usager = new Usager("doesntexist", "foobar@email.com", "bar", "foo", "+33 6 12 34 56 78", null, null);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(usager, headers);
        var responseEntity = restTemplate.exchange("/usagers/" + usager.getId(), HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void update_ShouldFail_IfEmailNotAvailable() {
        var email = "foobar@email.com";

        var u1 = new Usager(email, "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(u1);

        var u2 = new Usager("fizzbuzz@email.com", "buzz", "fizz", "+33 6 09 87 65 54");
        usagerService.save(u2);
        u2.setEmail(email);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(u2, headers);
        var responseEntity = restTemplate.exchange("/usagers/" + u2.getId(), HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    }

    @Test
    public void destroy_ShouldSucceed_IfRequestComplies() {
        var usager = new Usager("foobar@email.com", "bar", "foo", "+33 6 12 34 56 78");
        usagerService.save(usager);

        var requestEntity = new HttpEntity<>(null, new HttpHeaders());
        var responseEntity = restTemplate.exchange("/usagers/" + usager.getId(), HttpMethod.DELETE, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(usagerService.findById(usager.getId()).isPresent()).isFalse();
    }

}
