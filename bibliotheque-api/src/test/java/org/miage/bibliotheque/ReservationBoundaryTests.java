package org.miage.bibliotheque;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.Magazine;
import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.service.OeuvreService;
import org.miage.bibliotheque.service.ReservationService;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private UsagerService usagerService;

    @Autowired
    private OeuvreService oeuvreService;

    @Before
    public void reset() {
        oeuvreService.deleteAll();
        reservationService.deleteAll();
        usagerService.deleteAll();
    }

    @Test
    public void getAll_ShouldReturnAllReservations_IfAny() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        var reservation = reservationService.save(oeuvre.getId(), usager.getId());

        var responseEntity = restTemplate.getForEntity("/reservations", String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).contains(reservation.getId());
    }

    @Test
    public void store_ShouldSucceed_IfDataValid() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);

        var data = Map.of(
                "idOeuvre", oeuvre.getId(),
                "idUsager", usager.getId()
        );

        var responseEntity = restTemplate.postForEntity("/reservations", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(reservationService.findAll()).isNotEmpty();
    }

    @Test
    public void store_ShouldFail_IfOeuvreDoesntExist() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");

        var data = Map.of(
                "idOeuvre", "doesn't exist",
                "idUsager", usager.getId()
        );

        var responseEntity = restTemplate.postForEntity("/reservations", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfUsagerDoesntExist() {
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);

        var data = Map.of(
                "idOeuvre", oeuvre.getId(),
                "idUsager", "doesn't exist"
        );

        var responseEntity = restTemplate.postForEntity("/reservations", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfReservationAlreadyExist() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        reservationService.save(oeuvre.getId(), usager.getId());

        var data = Map.of(
                "idOeuvre", oeuvre.getId(),
                "idUsager", usager.getId()
        );

        var responseEntity = restTemplate.postForEntity("/reservations", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void destroy_ShouldSucceed_IfReservationExist() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        var reservation = reservationService.save(oeuvre.getId(), usager.getId());

        restTemplate.delete("/reservations/" + reservation.getId());

        assertThat(reservationService.findById(reservation.getId())).isEmpty();
    }

}
