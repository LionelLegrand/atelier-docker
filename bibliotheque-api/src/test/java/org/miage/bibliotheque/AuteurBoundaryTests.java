package org.miage.bibliotheque;

import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.service.AuteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
public class AuteurBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AuteurService auteurService;

    @Before
    public void reset() {
        auteurService.deleteAll();
    }

    @Test
    public void getAll_ShouldReturnAllAuteurs_IfAny() {
        var auteur = new Auteur(null, "Zola", "Emile", null);
        auteurService.save(auteur);

        var responseEntity = restTemplate.getForEntity("/auteurs", List.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotEmpty();
    }

    @Test
    public void store_ShouldSucceed_IfDataComplies() {
        var auteur = new Auteur(null, "Zola", "Emile", null);

        var responseEntity = restTemplate.postForEntity("/auteurs", auteur, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(auteurService.findAll()).isNotEmpty();
        assertThat(auteurService.findAll().get(0).getNom()).isEqualTo(auteur.getNom());
    }

    @Test
    public void store_ShouldFail_IfNullOrEmptyField() {
        var auteur = new Auteur(null, "", "Emile", null);
        var responseEntity = restTemplate.postForEntity("/auteurs", auteur, String.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(auteurService.findAll()).isEmpty();

        auteur = new Auteur(null, "Zola", null, null);
        responseEntity = restTemplate.postForEntity("/auteurs", auteur, String.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(auteurService.findAll()).isEmpty();
    }

    @Test
    public void update_ShouldSucceed_IfAuteurExist() {
        var auteur = auteurService.save(new Auteur(null, "Zola", "Emile", null));

        var nom = "Louis";
        auteur.setNom(nom);
        restTemplate.put("/auteurs/" + auteur.getId(), auteur);

        assertThat(auteurService.findById(auteur.getId()).get().getNom()).isEqualTo(nom);
    }

    @Test
    public void update_ShouldFail_IfAuteurDoesntExist() {
        var nom = "Zola";
        var auteur = auteurService.save(new Auteur(null, nom, "Emile", null));
        auteur.setNom("fail");

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(auteur, headers);
        var responseEntity = restTemplate.exchange("/auteurs/doesntexist", HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(auteurService.findById(auteur.getId()).get().getNom()).isEqualTo(nom);
    }

    @Test
    public void destroy_ShouldRemoveTheAuteur_IfItExists() {
        var auteur = auteurService.save(new Auteur(null, "Zola", "Emile", null));

        restTemplate.delete("/auteurs/" + auteur.getId());

        assertThat(auteurService.findById(auteur.getId())).isEmpty();
    }

}
