package org.miage.bibliotheque;

import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.EtatExemplaire;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.entity.Magazine;
import org.miage.bibliotheque.service.ExemplaireService;
import org.miage.bibliotheque.service.OeuvreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
public class ExemplaireBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ExemplaireService exemplaireService;

    @Autowired
    private OeuvreService oeuvreService;

    @Before
    public void reset() {
        exemplaireService.deleteAll();
    }

    @Test
    public void getAll_ShouldReturnAllExemplaires_IfAny() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = new Exemplaire(null, null, null, savedOeuvre);
        exemplaireService.save(exemplaire);

        var responseEntity = restTemplate.getForEntity("/exemplaires", List.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotEmpty();
    }

    @Test
    public void getOne_ShouldReturnTheExemplaire_IfItExist() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = new Exemplaire(null, null, null, savedOeuvre);
        exemplaire = exemplaireService.save(exemplaire);

        var responseEntity = restTemplate.getForEntity("/exemplaires/" + exemplaire.getId(), Exemplaire.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getId()).isEqualTo(exemplaire.getId());
    }

    @Test
    public void getOne_ShouldReturn404_IfExemplaireDoesntExist() {
        var responseEntity = restTemplate.getForEntity("/exemplaires/doesntexist", Exemplaire.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void store_ShouldSucceed_IfExemplaireDataIsValid() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var data = Map.of(
                "idOeuvre", savedOeuvre.getId(),
                "quantite", "1"
        );

        var responseEntity = restTemplate.postForEntity("/exemplaires", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(exemplaireService.findAll()).isNotEmpty();
        assertThat(exemplaireService.findAll().get(0).getOeuvre().getId()).isEqualTo(savedOeuvre.getId());
    }

    @Test
    public void store_ShouldSaveMany_IfQuantityGreaterThatOne() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var data = Map.of(
                "idOeuvre", savedOeuvre.getId(),
                "quantite", "8"
        );

        var responseEntity = restTemplate.postForEntity("/exemplaires", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(exemplaireService.findAll().size()).isEqualTo(8);
        assertThat(exemplaireService.findAll().get(0).getOeuvre().getId()).isEqualTo(savedOeuvre.getId());
    }

    @Test
    public void store_ShouldFail_IfOeuvreDoesntExist() {
        var data = Map.of(
                "idOeuvre", "doesn't exist",
                "quantite", "1"
        );

        var responseEntity = restTemplate.postForEntity("/exemplaires", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    @Transactional
    public void update_ShouldSucceed_IfDataComplies() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = new Exemplaire(null, null, null, savedOeuvre);
        exemplaire = exemplaireService.save(exemplaire);
        exemplaire.setEtat(EtatExemplaire.MAUVAIS);

        restTemplate.put("/exemplaires/" + exemplaire.getId(), exemplaire);

        assertThat(exemplaireService.findAll().get(0).getEtat()).isEqualTo(EtatExemplaire.MAUVAIS);
    }

    @Test
    public void update_ShouldFail_IfIdDoesntExist() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = new Exemplaire(null, null, null, savedOeuvre);
        exemplaire = exemplaireService.save(exemplaire);
        exemplaire.setEtat(EtatExemplaire.MAUVAIS);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(new Exemplaire(), headers);
        var responseEntity = restTemplate.exchange("/exemplaires/doesntexist", HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void update_ShouldFail_IfDataDoesntComply() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = new Exemplaire(null, null, null, savedOeuvre);
        exemplaire = exemplaireService.save(exemplaire);

        exemplaire.setEtat(EtatExemplaire.MAUVAIS);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var requestEntity = new HttpEntity<>(new Exemplaire(), headers);
        var responseEntity = restTemplate.exchange("/exemplaires/" + exemplaire.getId(), HttpMethod.PUT, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void destroy_ShouldSucceed_IfExemplaireExist() {
        var magazine = new Magazine(12);
        magazine.setTitre("onsenfout");
        var savedOeuvre = oeuvreService.save(magazine);
        var exemplaire = exemplaireService.save(new Exemplaire(null, null, null, savedOeuvre));

        restTemplate.delete("/exemplaires/" + exemplaire.getId());

        assertThat(exemplaireService.findById(exemplaire.getId()).isEmpty()).isTrue();
    }

}
