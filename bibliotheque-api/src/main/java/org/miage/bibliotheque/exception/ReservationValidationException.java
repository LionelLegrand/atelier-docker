package org.miage.bibliotheque.exception;

public class ReservationValidationException extends RuntimeException {

    public ReservationValidationException(String message) {
        super(message);
    }

}
