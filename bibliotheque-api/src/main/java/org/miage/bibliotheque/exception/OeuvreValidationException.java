package org.miage.bibliotheque.exception;

public class OeuvreValidationException extends RuntimeException {

    public OeuvreValidationException(String message) {
        super(message);
    }

}
