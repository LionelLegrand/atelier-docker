package org.miage.bibliotheque.exception;

public class EmpruntValidationException extends RuntimeException {

    public EmpruntValidationException(String message) {
        super(message);
    }

}
