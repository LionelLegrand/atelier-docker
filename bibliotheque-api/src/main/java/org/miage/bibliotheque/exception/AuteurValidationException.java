package org.miage.bibliotheque.exception;

public class AuteurValidationException extends RuntimeException {

    public AuteurValidationException(String message) {
        super(message);
    }

}
