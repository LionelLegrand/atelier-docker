package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Emprunt;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.entity.Oeuvre;
import org.miage.bibliotheque.entity.Reservation;
import org.miage.bibliotheque.exception.EmpruntValidationException;
import org.miage.bibliotheque.helper.EmpruntValidationHelper;
import org.miage.bibliotheque.repository.EmpruntRepository;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmpruntService {

    private final EmpruntRepository empruntRepository;

    private final EmpruntValidationHelper empruntValidationHelper;

    private final UsagerService usagerService;

    private final OeuvreService oeuvreService;

    private final ReservationService reservationService;

    public List<Emprunt> findAll() {
        return empruntRepository.findAll();
    }

    public Optional<Emprunt> findById(String id) {
        return empruntRepository.findById(id);
    }

    public Emprunt save(String idOeuvre, String idUsager, String dateDebut, String dateFin) {
        empruntValidationHelper.validate(dateDebut, dateFin, idUsager, idOeuvre);

        var oeuvre = oeuvreService.findById(idOeuvre).get();
        var usager = usagerService.findById(idUsager).get();
        var start = LocalDate.parse(dateDebut);
        var end = LocalDate.parse(dateFin);
        var exemplaire = findAvailableCopy(start, end, oeuvre);
        var emprunt = new Emprunt(UUID.randomUUID().toString(), start, end, usager, exemplaire);
        var reservationExample = new Reservation();
        reservationExample.setOeuvre(oeuvre);
        reservationExample.setUsager(usager);

        reservationService
                .findByExample(Example.of(reservationExample))
                .ifPresent(reservationService::delete);

        return empruntRepository.save(emprunt);
    }

    public void deleteById(String id) {
        empruntRepository.findById(id).ifPresent(empruntRepository::delete);
    }

    public void deleteAll() {
        empruntRepository.deleteAll();
    }

    private Exemplaire findAvailableCopy(LocalDate start, LocalDate end, Oeuvre oeuvre) {
        var available = true;

        for (Exemplaire copy : oeuvre.getExemplaires()) {
            for (Emprunt existing : copy.getEmprunts()) {
                var existingStart = existing.getDateDebut();
                var existingEnd = existing.getDateFin();
                available = !start.isBefore(existingEnd) || !existingStart.isBefore(end);
            }

            if (available) {
                return copy;
            }
        }

        throw new EmpruntValidationException("Aucun exemplaire disponible pour cette période.");
    }

}
