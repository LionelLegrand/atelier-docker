package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.helper.UsagerValidationHelper;
import org.miage.bibliotheque.repository.UsagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UsagerService {

    private final UsagerRepository usagerRepository;

    private UsagerValidationHelper usagerValidationHelper;

    @Autowired
    public void setUsagerValidationHelper(UsagerValidationHelper usagerValidationHelper) {
        this.usagerValidationHelper = usagerValidationHelper;
    }

    public List<Usager> findAll() {
        return usagerRepository.findAll();
    }

    public Optional<Usager> findById(String id) {
        return usagerRepository.findById(id);
    }

    public Optional<Usager> findByExample(Usager usager) {
        return usagerRepository.findOne(Example.of(usager));
    }

    public Usager save(Usager usager) {
        usager.setId(UUID.randomUUID().toString());
        usagerValidationHelper.validate(usager);

        return usagerRepository.save(usager);
    }

    public Usager update(String id, Usager usager) {
        usager.setId(id);
        usagerValidationHelper.validate(usager);

        return usagerRepository.save(usager);
    }

    public void delete(String id) {
        usagerRepository.findById(id).ifPresent(usagerRepository::delete);
    }

    public void deleteAll() {
        usagerRepository.deleteAll();
    }

}
