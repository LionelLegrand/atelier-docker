package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.exception.EmailNotAvailableException;
import org.miage.bibliotheque.exception.UsagerValidationException;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/usagers", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class UsagerBoundary {

    private final UsagerService usagerService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(usagerService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") String id) {
        return usagerService
                .findById(id)
                .map(usager -> new ResponseEntity<>(usager, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Usager usager) {
        try {
            usagerService.save(usager);
        } catch (UsagerValidationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EmailNotAvailableException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<?> update(@RequestBody Usager usager, @PathVariable("id") String id) {
        if (usagerService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        try {
            usagerService.update(id, usager);
        } catch (UsagerValidationException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (EmailNotAvailableException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> destroy(@PathVariable("id") String id) {
        usagerService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
