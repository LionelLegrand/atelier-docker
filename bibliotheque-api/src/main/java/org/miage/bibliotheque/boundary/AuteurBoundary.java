package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.exception.AuteurValidationException;
import org.miage.bibliotheque.service.AuteurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/auteurs", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class AuteurBoundary {

    private final AuteurService auteurService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(auteurService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Auteur auteur) {
        try {
            auteurService.save(auteur);
        } catch (AuteurValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<?> update(@RequestBody Auteur auteur, @PathVariable("id") String id) {
        if (auteurService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        try {
            auteurService.update(auteur, id);
        } catch (AuteurValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> destroy(@PathVariable("id") String id) {
        auteurService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
