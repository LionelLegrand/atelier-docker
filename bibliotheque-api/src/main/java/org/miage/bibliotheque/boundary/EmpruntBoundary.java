package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.exception.EmpruntValidationException;
import org.miage.bibliotheque.exception.ExemplaireValidationException;
import org.miage.bibliotheque.exception.UsagerValidationException;
import org.miage.bibliotheque.service.EmpruntService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/emprunts", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class EmpruntBoundary {

    private final EmpruntService empruntService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(empruntService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Map<String, String> data) {
        try {
            empruntService.save(data.get("idOeuvre"), data.get("idUsager"), data.get("dateDebut"), data.get("dateFin"));
        } catch (UsagerValidationException | ExemplaireValidationException | EmpruntValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> destroy(@PathVariable String id) {
        empruntService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
