package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "auteurs")
public class Auteur implements Serializable {

    @Id
    String id;

    String nom;

    String prenom;

    @ManyToMany(mappedBy = "auteurs", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties("auteurs")
    List<Livre> livres;

}
