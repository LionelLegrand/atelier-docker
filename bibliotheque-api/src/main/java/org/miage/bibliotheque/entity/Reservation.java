package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "reservations")
public class Reservation implements Serializable {

    @Id
    String id;

    LocalDate date;

    @ManyToOne
    @JoinColumn(name = "usager_id")
    @JsonIgnoreProperties("reservations")
    Usager usager;

    @ManyToOne
    @JoinColumn(name = "oeuvre_id")
    @JsonIgnoreProperties("reservations")
    Oeuvre oeuvre;

}
