package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "usagers")
public class Usager implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    private String id;

    private String email;

    private String nom;

    private String prenom;

    private String telephone;

    @OneToMany(mappedBy = "usager", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties("usager")
    List<Emprunt> emprunts;

    @OneToMany(mappedBy = "usager", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties("usager")
    List<Reservation> reservations;

    public Usager(String email, String nom, String prenom, String telephone) {
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
    }

}
