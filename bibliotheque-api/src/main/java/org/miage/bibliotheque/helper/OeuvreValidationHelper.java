package org.miage.bibliotheque.helper;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.entity.Livre;
import org.miage.bibliotheque.entity.Magazine;
import org.miage.bibliotheque.entity.Oeuvre;
import org.miage.bibliotheque.exception.OeuvreValidationException;
import org.miage.bibliotheque.service.AuteurService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OeuvreValidationHelper {

    private final AuteurValidationHelper auteurValidationHelper;

    private final AuteurService auteurService;

    public void validate(Oeuvre oeuvre) {
        if (oeuvre == null) {
            throw new OeuvreValidationException("L'oeuvre ne peut pas être nulle.");
        }

        if (oeuvre.getId() == null || oeuvre.getId().isBlank()) {
            throw new OeuvreValidationException("L'identifiant ne peut pas être vide.");
        }

        if (oeuvre.getTitre() == null || oeuvre.getTitre().isBlank()) {
            throw new OeuvreValidationException("Le titre ne peut pas être vide.");
        }

        if (oeuvre instanceof Livre) {
            validateLivre((Livre) oeuvre);
        }

        if (oeuvre instanceof Magazine) {
            validateMagazine((Magazine) oeuvre);
        }
    }

    private void validateMagazine(Magazine magazine) {
        if (magazine.getNumero() == null) {
            throw new OeuvreValidationException("Le numéro ne peut pas être vide.");
        }
    }

    private void validateLivre(Livre livre) {
        if (livre.getAuteurs() == null || livre.getAuteurs().isEmpty()) {
            throw new OeuvreValidationException("Le livre doit être liés à un ou plusieurs auteurs.");
        }

        for (Auteur auteur : livre.getAuteurs()) {
            auteurValidationHelper.validate(auteur);

            if (auteurService.findById(auteur.getId()).isEmpty()) {
                throw new OeuvreValidationException("Les auteurs du livres doivent exister dans la base de données.");
            }
        }

        if (livre.getEditeur() == null || livre.getEditeur().isBlank()) {
            throw new OeuvreValidationException("L'éditeur ne peut pas être vide.");
        }
    }

}
