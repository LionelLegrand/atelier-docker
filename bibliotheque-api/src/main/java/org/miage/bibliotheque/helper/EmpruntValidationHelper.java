package org.miage.bibliotheque.helper;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.exception.EmpruntValidationException;
import org.miage.bibliotheque.service.OeuvreService;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;

@Component
@RequiredArgsConstructor
public class EmpruntValidationHelper {

    private final UsagerService usagerService;

    private final OeuvreService oeuvreService;

    public void validate(String dateDebut, String dateFin, String idUsager, String idOeuvre) {
        LocalDate start;
        LocalDate end;

        try {
            start = LocalDate.parse(dateDebut);
        } catch (DateTimeParseException | NullPointerException e) {
            throw new EmpruntValidationException("Format de date incorrect pour la date de début.");
        }

        try {
            end = LocalDate.parse(dateFin);
        } catch (DateTimeParseException | NullPointerException e) {
            throw new EmpruntValidationException("Format de date incorrect pour la date de fin. ");
        }

        if (Period.between(start, end).isNegative()) {
            throw new EmpruntValidationException("La date de début de l'emprunt doit être antérieur à la date de fin.");
        }

        if (idUsager == null) {
            throw new EmpruntValidationException("L'identifiant de l'usager ne peut être vide.");
        }

        if (usagerService.findById(idUsager).isEmpty()) {
            throw new EmpruntValidationException("Usager inconnu : " + idUsager);
        }

        if (idOeuvre == null) {
            throw new EmpruntValidationException("L'identifiant de l'oeuvre ne peut être vide.");
        }

        if (oeuvreService.findById(idOeuvre).isEmpty()) {
            throw new EmpruntValidationException("Oeuvre inconnue : " + idOeuvre);
        }
    }

}
