package org.miage.bibliotheque.helper;

import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.exception.EmailNotAvailableException;
import org.miage.bibliotheque.exception.UsagerValidationException;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class UsagerValidationHelper implements ApplicationContextAware, InitializingBean {

    private UsagerService usagerService;

    private ApplicationContext context;

    @Override
    public void afterPropertiesSet() {
        usagerService = context.getBean(UsagerService.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    private boolean emailNotAvailable(Usager usager) {
        return usagerService.findAll()
                .stream()
                .anyMatch(u -> !u.getId().equals(usager.getId()) && u.getEmail().equals(usager.getEmail()));
    }

    public void validate(Usager usager) {
        if (usager == null) {
            throw new UsagerValidationException("L'usager ne peut pas être nul.");
        }

        if (usager.getId() == null || usager.getId().isBlank()) {
            throw new UsagerValidationException("L'identifiant ne peut pas être vide.");
        }

        if (usager.getEmail() == null || usager.getEmail().isBlank()) {
            throw new UsagerValidationException("L'email ne peut être vide.");
        }

        if (emailNotAvailable(usager)) {
            throw new EmailNotAvailableException();
        }

        if (usager.getNom() == null || usager.getNom().isBlank()) {
            throw new UsagerValidationException("Le nom ne peut pas être vide.");
        }

        if (usager.getPrenom() == null || usager.getPrenom().length() < 2) {
            throw new UsagerValidationException("Le prénom ne peut pas être vide.");
        }

        if (usager.getTelephone() == null || usager.getTelephone().length() < 10) {
            throw new UsagerValidationException("Le numéro de téléphone doit contenir au moins 10 caractères.");
        }

    }

}
