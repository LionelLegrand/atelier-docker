package org.miage.bibliotheque.helper;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.exception.ExemplaireValidationException;
import org.miage.bibliotheque.service.OeuvreService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ExemplaireValidationHelper {

    private final OeuvreService oeuvreService;

    private final OeuvreValidationHelper oeuvreValidationHelper;

    public void validate(Exemplaire exemplaire) {
        if (exemplaire == null) {
            throw new ExemplaireValidationException("L'auteur ne peut pas être nul.");
        }

        if (exemplaire.getId() == null || exemplaire.getId().isBlank()) {
            throw new ExemplaireValidationException("L'identifiant ne peut pas être vide.");
        }

        if (exemplaire.getEtat() == null) {
            throw new ExemplaireValidationException("L'état ne peut être que 'BON' ou 'MAUVAIS'.");
        }

        oeuvreValidationHelper.validate(exemplaire.getOeuvre());

        if (oeuvreService.findById(exemplaire.getOeuvre().getId()).isEmpty()) {
            throw new ExemplaireValidationException("L'exemplaire doit être lié à une oeuvre existante.");
        }
    }

    public void validate(String idOeuvre, String quantite) {
        if (idOeuvre == null) {
            throw new ExemplaireValidationException("L'identifiant de l'oeuvre ne peut pas être vide.");
        }

        if (oeuvreService.findById(idOeuvre).isEmpty()) {
            throw new ExemplaireValidationException("Oeuvre inconnue : " + idOeuvre);
        }

        int quantity;

        try {
            quantity = Integer.parseInt(quantite);
        } catch (NumberFormatException e) {
            throw new ExemplaireValidationException("Format invalide : la quantité doit être un entier.");
        }

        if (quantity <= 0) {
            throw new ExemplaireValidationException("La quantité doit être supérieure à zéro.");
        }
    }
}
