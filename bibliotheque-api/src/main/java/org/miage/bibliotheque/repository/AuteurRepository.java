package org.miage.bibliotheque.repository;

import org.miage.bibliotheque.entity.Auteur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuteurRepository extends JpaRepository<Auteur, String> {
}
